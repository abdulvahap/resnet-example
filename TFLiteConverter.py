import tensorflow as tf
import os


# Recreate the exact same model, including its weights and the optimizer
model = tf.keras.models.load_model('ResNet50.h5')

# Convert the model.
converter = tf.lite.TFLiteConverter.from_keras_model(model)
converter.optimizations = [tf.lite.Optimize.DEFAULT]
tflite_model = converter.convert()

# Save the model.
with open('ResNet50.tflite', 'wb') as f:
  f.write(tflite_model)

os.system("xxd -i ResNet50.tflite > ResNet50_lite.cpp")

print("Done!")
