from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np

model = ResNet50(
    include_top=True, weights='imagenet', input_tensor=None,
    input_shape=None, pooling=None, classes=1000)

img_path = 'dog.jpg'
img = image.load_img(img_path, target_size=(224, 224))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)

preds = model.predict(x)

# save the network to disk
print("[INFO] saving network ...")
# model.save("ResNet50.h5")
# decode the results into a list of tuples (class, description, probability)
# (one such list for each sample in the batch)
print(preds.shape)
print('Predicted:', decode_predictions(preds, top=3)[0])
# Predicted: [(u'n02504013', u'Indian_elephant', 0.82658225),
# (u'n01871265', u'tusker', 0.1122357),
# (u'n02504458', u'African_elephant', 0.061040461)]
